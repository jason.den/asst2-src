/*
 * Declarations for file handle and file table management.
 */

#ifndef _FILE_H_
#define _FILE_H_

/*
 * Contains some file-related maximum length constants
 */
#include <limits.h>
#include <synch.h>
#include <vnode.h>

/*
 * Put your function declarations and data types here ...
 */

struct file_descriptor {
    int             index;
    int             mode;
};

struct opened_file {
    struct vnode*   vn;
    off_t           offset;
    int             refcount;
    struct lock*    lock;
};

struct opened_file** of_table;

struct lock* dup_lock;

int sys_open(const char* filename, int flags, mode_t mode, int* err);
int sys_close(int fd, int* err);

ssize_t sys_read(int fd, void *buf, size_t nbytes, int* err);
ssize_t sys_write(int fd, const void *buf, size_t nbytes, int* err);

off_t sys_lseek(int fd, uint64_t offset, int whence, int* err);
int sys_dup2(int from, int to, int* err);

#endif /* _FILE_H_ */
