#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <syscall.h>
#include <copyinout.h>
#include <proc.h>

#define TABLE_MAX OPEN_MAX*2

/*
    create a opened_file;
    return 0 when success; err code otherwise;
*/
int of_create(const char* filename, int flags, mode_t mode, struct opened_file** of) {
    
    // guard vfs_open 
    struct vnode* new_node;
    int open_err = vfs_open((char*)filename, flags, mode, &new_node);
    if (open_err) {
        return open_err;
    }

    // guard malloc
    *of = kmalloc(sizeof(struct opened_file));
    if (*of == NULL) {
        vfs_close(new_node);
        kfree(new_node);
        return ENOMEM;
    }

    // create success
    (*of)->vn = new_node;
    (*of)->offset = 0;
    (*of)->refcount = 1;
    (*of)->lock = lock_create("open file table lock");

    return 0;
}


/*
    inititialize files 
*/
int file_init(void) {
    // init open file table
    of_table = kmalloc(sizeof(struct opened_file*)*TABLE_MAX);
    if (of_table == NULL) {
        return ENOMEM;
    }
    memset(of_table, 0, sizeof(struct opened_file*)*TABLE_MAX);
    
    // connect to std(in/out/err)
    int std_mode = 0644;
    char con[] = "con:";
    int r0 = of_create(con, O_RDONLY, std_mode, &of_table[0]);
    int r1 = of_create(con, O_WRONLY, std_mode, &of_table[1]);
    int r2 = of_create(con, O_WRONLY, std_mode, &of_table[2]);
    if (r0) return r0;
    if (r1) return r1;
    if (r2) return r2;
    of_table[0]->refcount = 0;
    of_table[1]->refcount = 0;
    of_table[2]->refcount = 0;
    
    // init dup lock 
    dup_lock = lock_create("dup_lock");
    if (dup_lock == NULL) {
        return ENOMEM;
    }
    
    // init fd_table for curproc
    memset(curproc->fd_table, 0, sizeof(struct file_descriptor)*OPEN_MAX);
    of_table[0]->refcount++;
	curproc->fd_table[0].index = 0;
	curproc->fd_table[0].mode = std_mode;
    of_table[1]->refcount++;
	curproc->fd_table[1].index = 1;
	curproc->fd_table[1].mode = std_mode;
    of_table[2]->refcount++;
	curproc->fd_table[2].index = 2;
	curproc->fd_table[2].mode = std_mode;

    return 0;
}


/*
    open file;
    return -1 when fails, return >2 otherwise
*/
int sys_open(const char* filename, int flags, mode_t mode, int* err) {
    // check args
    //      filename
    if (filename == NULL ||  strlen(filename) == 0) {
        *err = EFAULT;
        return -1;
    }

    //      flags
    if (flags & O_ACCMODE == O_ACCMODE) {
        *err = EINVAL;
        return -1;
    }

    //      check of_table capacity
    int index;
    for (index=0; index<TABLE_MAX; index++) {
        if (of_table[index]==0) {
            break;
        }
    }
    if (index == TABLE_MAX) {
        *err = ENFILE;
        return -1;
    }

    // opening file

    int create_err = of_create(filename, flags, mode, &of_table[index]);
    if (create_err) {
        *err = create_err;
        return -1;
    }
    
    if (flags & O_APPEND) {
        struct stat info;
        off_t err_ret;
        err_ret = VOP_STAT(of_table[index]->vn, &info);
        if (err_ret) {
            *err = err_ret;
            return -1;
        }

        of_table[index]->offset = info.st_size;
    }

    //  append 
    int fd_table_idx;
    for (fd_table_idx=3; fd_table_idx<OPEN_MAX; fd_table_idx++)
        if (curproc->fd_table[fd_table_idx].index == 0) {
            curproc->fd_table[fd_table_idx].index = index;
            curproc->fd_table[fd_table_idx].mode  = flags & O_ACCMODE;
            return fd_table_idx;
        }

    // full fd_table
    *err = EMFILE;
    return -1;
}

/*
    close file;
    return -1 when fails
*/
int sys_close(int fd, int* err) {
    // enter critical 
    lock_acquire(dup_lock);
    
    // guard args validity
    if (
        (fd<3 || fd>=OPEN_MAX) ||
        (curproc->fd_table[fd].index==0) ||
        (of_table[curproc->fd_table[fd].index]==NULL) 
    ) {
        lock_release(dup_lock); // end critical 
        *err = EBADF;
        return -1;
    }

    // decrease refcount
    of_table[curproc->fd_table[fd].index]->refcount--;

    // if no one opening the file  (refcount become 0)
    if (of_table[curproc->fd_table[fd].index]->refcount==0) {
        vfs_close(of_table[curproc->fd_table[fd].index]->vn);

        // fd
        curproc->fd_table[fd].index = 0;
        curproc->fd_table[fd].mode = 0;

        // of
        lock_destroy(of_table[curproc->fd_table[fd].index]->lock);
        of_table[curproc->fd_table[fd].index]->vn = NULL;
        of_table[curproc->fd_table[fd].index] = NULL;
    }

    lock_release(dup_lock); // end critical 
    return 0;
}

/*
    read file;
    return -1 when fails
    return readed bytes when success
*/
ssize_t sys_read(int fd, void *buf, size_t nbytes, int* err) {
    // guard args validity
    //      fd
	if (fd < 0 || fd >= OPEN_MAX) {
        *err = EBADF;
        return -1;
    }

    //      buffer; nbytes
    if (buf == NULL || nbytes < 0) {
        *err = EFAULT;
        return -1;
    }


	struct file_descriptor * fd = &(curproc->fd_table[fd]);
	struct opened_file * of = of_table[fd->index];
    // check of_table 
    if(of == NULL) {
        *err = EBADF;
        return -1;
    }

	//  mode
	if(fd->mode == O_WRONLY) { // TODO: find better practice
        *err = EBADF;
        return -1;
    }

	void * kbuf = kmalloc(sizeof(void *) * nbytes);
	if(kbuf == NULL) {
        *err = EFAULT;
        return -1;
    }

    // reading process
	struct iovec _iovec;
	_iovec.iov_ubase = (userptr_t) buf;
	struct uio _uio;

	lock_acquire(of->lock);         //  enter critical area (for of->offset)

	uio_kinit(&_iovec, &_uio, kbuf, nbytes, of->offset, UIO_READ);

    //      read 
	int read_err = VOP_READ(of->vn, &_uio);
	if (read_err) {
		lock_release(of->lock);
		kfree(kbuf);
        *err = read_err;
		return -1;
	}

    int len = nbytes - _uio.uio_resid;    
    //      copy: from kernel buffer to user buffer
    int copy_err = copyout(kbuf, buf, len);
	kfree(kbuf);

    copy_err = copy_err && len
    if (copy_err) {  
        lock_release(of->lock);     // eixt critical area
        *err = copy_err;
        return -1;
    }

	of->offset += len;              // update of->offset
	lock_release(of->lock);         // eixt critical area

	return len;
}


/*
    read file;
    return <0 when fails
    return wrote bytes when success
*/
ssize_t sys_write(int fd, const void *buf, size_t nbytes, int* err) {
    // guard args validity
	//      fd
	if (fd < 0 || fd >= OPEN_MAX) {
        *err = EBADF;
        return -1;
    }

    //      buffer & nbytes
    if (buf == NULL || nbytes < 0) {
        *err = EFAULT;
        return -1;
    }

    struct file_descriptor * fd = &(curproc->fd_table[fd]);
	struct opened_file * of = of_table[fd->index];
    // check of_table 
    if(of == NULL) {
        *err = EBADF;
        return -1;
    }

	// check mode
	if(fd->mode == O_WRONLY) { // TODO: find better practice
        *err = EBADF;
        return -1;
    }

	void * kbuf = kmalloc(sizeof(void *) * nbytes);
	if(kbuf == NULL) {
        *err = EFAULT;
        return -1;
    }

    //copy the userland buffer to kernal buffer
	int copy_err = copyin((const_userptr_t) buf, kbuf, nbytes);
	if (copy_err && nbytes)
	{
		kfree(kbuf);
        *err = copy_err;
		return -1;
	}

    // writing process
	struct iovec _iovec;
	_iovec.iov_ubase = (userptr_t) buf;
	struct uio _uio;

	lock_acquire(of->lock);     //  enter critical area (for of->offset)

	uio_kinit(&_iovec, &_uio, kbuf, nbytes, of->offset, UIO_WRITE);
	
    //      write
    int write_err = VOP_WRITE(of->vn, &_uio);
	if (write_err)
	{
		lock_release(of->lock);
		kfree(kbuf);
        *err = write_err;
		return -1;
	}

    int len = nbytes - _uio.uio_resid;
    
    of->offset += len;              // update of->offset
	lock_release(of->lock);         // eixt critical area
	kfree(kbuf);

	return len;
}


/*
    lseek file;
    return val < 0 when fails
    return offset bytes when success
*/
off_t sys_lseek(int fd, uint64_t offset, int whence, int* err) {
    // guard args validity
    if (
        (fd<0 || fd>=OPEN_MAX) ||
        (fd!=0 && curproc->fd_table[fd].index==0) ||
        (of_table[curproc->fd_table[fd].index]==NULL) 
    ) {
        *err = EBADF;   /* Bad file number */
        return -1;
    }
    if (
        (fd<3 || fd>=OPEN_MAX) ||
        (curproc->fd_table[fd].index==0) ||
        (of_table[curproc->fd_table[fd].index]==NULL) 
    ) {
        *err = ESPIPE;  /* Illegal seek */
        return -1;
    }

    // check seekable
    if (!VOP_ISSEEKABLE(of_table[curproc->fd_table[fd].index]->vn)) {
        *err = ESPIPE;
        return -1;
    }

    struct stat info;
    off_t ret;
    ret = VOP_STAT(of_table[curproc->fd_table[fd].index]->vn, &info);
    if (ret) {
        *err = ret;
        return -1;
    }

    // TODO: implement it 
    return ret;
}


/*
    copy file;
    return val < 0 when fails
    return 0 when success
*/
int sys_dup2(int from, int to, int* err) {
    lock_acquire(dup_lock);
    // guard args validity
    //      from, to
    if (
        (from<0 || from>=OPEN_MAX) ||
        (from!=0 && curproc->fd_table[from].index==0) ||
        (of_table[curproc->fd_table[from].index]==NULL) ||
        to<0 || to >= OPEN_MAX
    ) {
        lock_release(dup_lock);
        *err = EBADF;
        return -1;
    }

    if (from==to) {
        lock_release(dup_lock);
        return 0;
    }
    
    // dup2 
    if ((to>2 && curproc->fd_table[to].index==0) || to<3 ) {

        of_table[curproc->fd_table[to].index]->refcount--;
        if (of_table[curproc->fd_table[to].index]->refcount==0) {

            vfs_close(of_table[curproc->fd_table[to].index]->vn);
            
            of_table[curproc->fd_table[to].index]->vn = NULL;
            lock_destroy(of_table[curproc->fd_table[to].index]->lock);
            of_table[curproc->fd_table[to].index] = NULL;
        }
    } else {
        // simple copy
        curproc->fd_table[to].index = curproc->fd_table[from].index;
        curproc->fd_table[to].mode = curproc->fd_table[from].mode;

        of_table[curproc->fd_table[to].index]->refcount++;
        lock_release(dup_lock);
    }

    return to;
}
