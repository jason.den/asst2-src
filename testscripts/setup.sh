# Configure OS/161 for Assignment 2
#   Before proceeding further, configure your new sources.
cd ~/cs3231/asst2-src
./configure

#   Unlike previous the previous assignment, you will need to build and install the user-level programs that will be run by your kernel in this assignment.
cd ~/cs3231/asst2-src
bmake
bmake install


#   For your kernel development, again we have provided you with a framework for you to run your solutions for ASST2.
#   You have to reconfigure your kernel before you can use this framework. The procedure for configuring a kernel is the same as in ASST0 and ASST1, except you will use the ASST2 configuration file:
cd ~/cs3231/asst2-src/kern/conf
./config ASST2

# Building for ASST2
#   When you built OS/161 for ASST1, you ran make from compile/ASST1 . In ASST2, you run make from (you guessed it) compile/ASST2.

cd ../compile/ASST2
bmake depend
bmake
bmake install


# Note: If you don't have a sys161.conf file, you can use the one from ASST1.
# The simplest way to install it is as follows:
cd ~/cs3231/root
wget http://cgi.cse.unsw.edu.au/~cs3231/19T1/assignments/asst1/sys161.conf -O sys161.conf
